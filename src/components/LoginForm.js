import { useContext, useRef, useState } from 'react'
import { supabase } from '../utils/supabaseClient'
import { UserSessionContext } from '../store/UserSessionContext'

export default function LoginForm() {
  const { setUserSession } = useContext(UserSessionContext)

  const [loading, setLoading] = useState(false)
  const [createNewAccount, setCreateNewAccount] = useState(null)
  const email = useRef('')
  const password = useRef('')

  const handleSignup = async (event) => {
    event.preventDefault()

    try {
      setLoading(true)

      const { user, session, error } = await supabase.auth.signUp({
        email: email.current.value,
        password: password.current.value
      })

      if (error) throw error

      setUserSession(user)
    } catch (error) {
      console.error(error)

      alert('Error creating account, bro.')
    }
    setLoading(false)
  }

  const handleSignIn = async (event) => {
    event.preventDefault()

    try {
      setLoading(true)
      // const { refresh_token } = supabase.auth.session()

      const { user, session, error } = await supabase.auth.signIn({
        // refreshToken: refresh_token
        email: email.current.value,
        password: password.current.value
      })

      console.info(session)

      if (error) throw error

      setUserSession({
        user: user,
        session: session
      })
      setLoading(false)
    } catch (error) {
      console.error(error)

      alert('Error signing into your account, bro.')
      setLoading(false)
    }
  }

  const Form = () => {
    return (
      <div>
        <p>
          {createNewAccount
            ? 'Sign up for new account'
            : 'Sign in to existing account'}
        </p>
        <form onSubmit={createNewAccount ? handleSignup : handleSignIn}>
          <label>Email</label>
          <input type="email" ref={email} />

          <label>Password</label>
          <input type="password" ref={password} />

          <button disabled={loading}>
            {createNewAccount ? 'Sign up!' : 'Sign in!'}
          </button>
        </form>
      </div>
    )
  }

  return (
    <div>
      {createNewAccount === null ? (
        <div>
          <button onClick={() => setCreateNewAccount(true)}>
            Create account?
          </button>
          <button onClick={() => setCreateNewAccount(false)}>
            Log into existing account?
          </button>
        </div>
      ) : (
        <Form />
      )}
    </div>
  )
}
