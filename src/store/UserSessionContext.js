import { createContext } from 'react'
import { supabase } from '../utils/supabaseClient'

export const UserSessionContext = createContext(null)
