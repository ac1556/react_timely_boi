import logo from './logo.svg'
import './App.css'
import './components/LoginForm'
import LoginForm from './components/LoginForm'

import { useState, useMemo, useEffect } from 'react'
import { UserSessionContext } from './store/UserSessionContext'
import { supabase } from './utils/supabaseClient'

function App() {
  const [userSession, setUserSession] = useState(null)
  const providerUser = useMemo(
    () => ({
      userSession: userSession,
      setUserSession
    }),
    [userSession, setUserSession]
  )

  return (
    <div className="App">
      <UserSessionContext.Provider value={providerUser}>
        <LoginForm />
      </UserSessionContext.Provider>
    </div>
  )
}

export default App
